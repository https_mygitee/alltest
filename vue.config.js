const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
    // 1、服务器代理 
    devServer: {
      proxy: {
        '/api': {  //匹配所有以/api开头的路径
          target: 'http://localhost:8081', //代理目标的基础路径
          pathRewrite: { '^/api': '' } ,//修改最终路径删除/api
          ws: true, //用于支持websocket
          changeOrigin: true //用于控制请求头中的host值
        },
      }
    }
})
